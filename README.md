# Skripsi

Pemanfaatan SDN untuk deteksi aktivitas Port Scanning \
Beberapa hal yang dilakukan:

1. Konfigurasi topologi jaringan SDN sederhana dengan Faucet Controller
2. Aplikasi untuk deteksi port-scanning ada di `monitor.py` dengan memanfaatkan library `pyshark`

## Pre-requisites

1. Download faucet-amd64-VERSION.qcow2 dari https://github.com/faucetsdn/faucet/releases/latest
   Keterangan: latest saat tutorial dibuat = v1.9.52
   `wget https://github.com/faucetsdn/faucet/releases/download/1.9.52/faucet-amd64-1.9.52.qcow2`
2. Virtualbox tidak bisa membaca file qcow2, maka harus di convert dengan
   `qemu-img convert -f qcow2 faucet-amd64-1.9.52.qcow2 -O vdi faucet-amd64-1.9.52.vdi`
3. VM Kali Linux (untuk attacker)

## Set Up Ubuntu untuk Faucet Controller

1. Install kebutuhan Faucet

```
sudo apt-get install curl gnupg apt-transport-https lsb-release
echo "deb https://packagecloud.io/faucetsdn/faucet/$(lsb_release -si | awk '{print tolower($0)}')/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/faucet.list
curl -L https://packagecloud.io/faucetsdn/faucet/gpgkey | sudo apt-key add -
sudo apt-get update
sudo apt-get install faucet-all-in-one
```

2. Tambahkan argumen pada prometheus

```
sudo vim /etc/default/prometheus
# Set the command-line arguments to pass to the server.
ARGS="--config.file=/etc/faucet/prometheus/prometheus.yml"
```

3. Restart Prometheus & Configure Grafana

```
sudo systemctl restart prometheus
sudo systemctl daemon-reload
sudo systemctl enable grafana-server
sudo systemctl start grafana-server
```

4. Login ke [http://localhost:3000](http://localhost:3000) dengan credential

```
Username: admin
Password: admin
```

5. Tambah dashboard:
   - [Instrumentation](https://docs.faucet.nz/en/latest/_static/grafana-dashboards/faucet_instrumentation.json)
   - [Inventory](https://docs.faucet.nz/en/latest/_static/grafana-dashboards/faucet_inventory.json)
   - [Port Statisctics](https://docs.faucet.nz/en/latest/_static/grafana-dashboards/faucet_port_statistics.json)
6. Configure Faucet
   Lihat Bagian Topologi Jaringan.

## Topologi Jaringan

Host 1:

```
NAMA        : pc1
NAMESPACE   : faucet-pc1
TIPE        : Network Namespace
KEGUNAAN    : TARGET
IP ADDRESS  : 192.168.0.1
NETMASK     : /24
INTERFACE   : veth0 & lo
ATTACHED TO : veth-pc1
```

Host 2:

```
NAMA        : Metasploitable
TIPE        : Virtual Machine
KEGUNAAN    : TARGET
IP ADDRESS  : 192.168.0.2
NETMASK     : /24
INTERFACE   : eth0 & lo
ATTACHED TO : veth-server
```

Host 3:

```
NAMA        : Kali Linux
TIPE        : Virtual Machine
KEGUNAAN    : ATTACKER
IP ADDRESS  : 192.168.0.3
NETMASK     : /24
INTERFACE   : lo
INTERFACE   : eth0
ATTCHED TO  : veth-kali
```

Openflow Switch:

```
NAMA        : br0
TIPE        : bridge
KEGUNAAN    : TARGET
IP ADDRESS  : 192.168.0.250
NETMASK     : /24
INTERCACE   : veth-pc1 (terhubung dengan pc1)
INTERCACE   : veth-server (bridge dengan VM Metasploitable)
INTERCACE   : veth-pc2 (bridge dengan VM Kali Linux)
INTERCACE   : br0
Controller  : br0 (terhubung dengan tcp:127.0.0.1:6653 & tcp:127.0.0.1:6654 )
```

Laptop

```
NAMA        : Laptop
TIPE        : Real
KEGUNAAN    : CONTROLLER
IP ADDRESS  : 192.168.100.128
NETMASK     : /24
INTERACE    : lo
INTERACE    : enps03 (192.168.100.128)
```

## Konfigurasi Faucet

1. /etc/faucet/faucet.yaml

```
vlans:
  default:
    vid: 1
    description: "default vlan"

dps:
  sw:
    dp_id: 0x1
    hardware: "Open vSwitch"
    interfaces:
      1:
        name: "PC 1"
        description: "Target"
        native_vlan: default
      2:
        name: "Server"
        description: "Target"
        native_vlan: default
        acls_in: [block-tcp,block-udp,allow-all]
      3:
        name: "Kali"
        description: "Attacker"
        native_vlan: default

acls:
  block-ping:
    - rule:
      dl_type: 0x800      # IPv4
      ip_proto: 1         # ICMP
      actions:
        allow: False
  block-tcp:
    - rule:
      dl_type: 0x800      # IPv4
      nw_proto: 6         # TCP
      actions:
          allow: False
  block-udp:
    - rule:
      dl_type: 0x800      # IPv4
      nw_proto: 17        # UDP
      actions:
        allow: False
  block-by-ip:
    - rule:
      dl_type: 0x800      # IPv4
      actions:
        allow: False
  allow-all:
    - rule:
        actions:
          allow: True
```

2. Check konfigurasi faucet
   `check_faucet_config /etc/faucet/faucet.yaml`
3. `sudo systemctl reload faucet`
4. /bin/functions.sh

```
# Run command inside network namespace
as_ns () {
    NAME=$1
    NETNS=faucet-${NAME}
    shift
    sudo ip netns exec ${NETNS} $@
}

# Create network namespace
create_ns () {
    NAME=$1
    IP=$2
    NETNS=faucet-${NAME}
    sudo ip netns add ${NETNS}
    sudo ip link add dev veth-${NAME} type veth peer name veth0 netns ${NETNS}
    sudo ip link set dev veth-${NAME} up
    as_ns ${NAME} ip link set dev lo up
    [ -n "${IP}" ] && as_ns ${NAME} ip addr add dev veth0 ${IP}
    as_ns ${NAME} ip link set dev veth0 up
}

# Clean up namespaces, bridges and processes created during faucet tutorial
cleanup () {
    for NETNS in $(sudo ip netns list | grep "faucet-" | awk '{print $1}'); do
        [ -n "${NETNS}" ] || continue
        NAME=${NETNS#faucet-}
        if [ -f "/run/dhclient-${NAME}.pid" ]; then
            # Stop dhclient
            sudo pkill -F "/run/dhclient-${NAME}.pid"
        fi
        if [ -f "/run/iperf3-${NAME}.pid" ]; then
            # Stop iperf3
            sudo pkill -F "/run/iperf3-${NAME}.pid"
        fi
        if [ -f "/run/bird-${NAME}.pid" ]; then
            # Stop bird
            sudo pkill -F "/run/bird-${NAME}.pid"
        fi
        # Remove netns and veth pair
        sudo ip link delete veth-${NAME}
        sudo ip netns delete ${NETNS}
    done
    for isl in $(ip -o link show | awk -F': ' '{print $2}' | grep -oE "^l-br[0-9](_[0-9]*)?-br[0-9](_[0-9]*)?"); do
        # Delete inter-switch links
        sudo ip link delete dev $isl 2>/dev/null || true
    done
    for DNSMASQ in /run/dnsmasq-vlan*.pid; do
        [ -e "${DNSMASQ}" ] || continue
        # Stop dnsmasq
        sudo pkill -F "${DNSMASQ}"
    done
    # Remove faucet dataplane connection
    sudo ip link delete veth-faucet 2>/dev/null || true
    # Remove openvswitch bridges
    sudo ovs-vsctl --if-exists del-br br0
    sudo ovs-vsctl --if-exists del-br br1
    sudo ovs-vsctl --if-exists del-br br2
    sudo ovs-vsctl --if-exists del-br br3
}
```

5. ~/.bashrc
   `source bin/functions.sh`
6. `source ~/.bashrc`

## Konfigurasi Jaringan

Pada Faucet:

1. Cek apakah ada type dummy untuk menambahkan virtual port ethernet

```
sudo lsmod | grep dummy
sudo modprobe dummy
sudo lsmod | grep dummy
```

2. Tambah port ethernet untuk Kali Linux dan Metasploitable pada Ubuntu base

```
sudo ip link add dev veth-kali type dummy
sudo ip link add dev veth-server type dummy
sudo ip link set veth-kali up
sudo ip link set veth-server up
```

3. Set VM Kali Linux Network Adapter `bridge` to `veth-kali`
4. Set VM Metasploitable Network Adapter `bridge` to `veth-server`
5. Membuat 1 host (dengan namespace `faucet-pc1`) dengan nama pc1 serta konfigurasi OpenvSwitch (ditulis dalam file `script.sh`):

```
source bin/functions.sh

create_ns pc1 192.168.0.1/24

sudo ovs-vsctl add-br br0 \
-- set bridge br0 other-config:datapath-id=0000000000000001 \
-- set bridge br0 other-config:disable-in-band=true \
-- set bridge br0 fail_mode=secure \
-- add-port br0 veth-pc1 -- set interface veth-pc1 ofport_request=1 \
-- add-port br0 veth-server -- set interface veth-server ofport_request=2 \
-- add-port br0 veth-kali -- set interface veth-kali ofport_request=3 \
-- set-controller br0 tcp:127.0.0.1:6653 tcp:127.0.0.1:6654
```

6. `sudo chmod +x script.sh`
7. `./script.sh`
8. Jalankan VM Kali Linux dan Metasploitable
9. Set IP Kali Linux 192.168.0.3
10. Set IP Metasploitable 192.168.0.2

## Troubleshooting

1. Melihat konfigurasi overflow switch: `sudo ovs-vsctl show`
2. Set IP Address: `sudo ip addr add [IP ADDRESS]/[NETMASK] dev [INTERFACE]`
3. Enabling suatu interface: `sudo ip link set [INTERFACE] up`
4. Menjalankan command pada namespace: `sudo ip netns exec [NAMESPACE] [COMMAND LINUX BIASA]` atau `as_ns [NAMA HOST] [COMMAND LINUX BIASA]`
5. Melihat routing table: `netstat -rn`
6. Menambahkan routing table: `sudo ip route add [IP TUJUAN]/[NETMASK] via [IP NEXT HOP]`
7. Menambahkan default routing: `sudo ip route add default via [IP NEXT HOP] dev [INTERFACE HOST NYA (BUKAN INTERFACE NEXT HOP NYA)]`

## Saat melanjutkan skripsi

Saat melanjutkan skripsi, takutnya ada konfigurasi yang hilang..berikut yang harus dilakukan

1. Cek apakah ada type dummy untuk menambahkan virtual port ethernet

```
sudo lsmod | grep dummy
sudo modprobe dummy
sudo lsmod | grep dummy
```

2. Tambah port ethernet untuk Kali Linux dan Metasploitable pada Ubuntu base

```
sudo ip link add dev veth-kali type dummy
sudo ip link add dev veth-server type dummy
sudo ip link set veth-kali up
sudo ip link set veth-server up
```

3. Set VM Kali Linux Network Adapter `bridge` to `veth-kali`
4. Set VM Metasploitable Network Adapter `bridge` to `veth-server`
5. `create_ns pc1 192.168.0.1/24`
6. Jalankan VM Kali Linux dan Metasploitable
7. Set IP Kali Linux 192.168.0.3
8. Set IP Metasploitable 192.168.0.2

## Grafana Plugins

`sudo grafana-cli plugins install jdbranham-diagram-panel`

plugin ditaruh di `var/lib/grafana/plugins/`. \
Pastikan memiliki akses write `sudo chmod 777 -R /var/lib/grafana/plugins/`

Description Graph:

```
graph TB
    A[OpenvSwitch] --- B[PC 1] & C[Server] & D[PC 2]
```
