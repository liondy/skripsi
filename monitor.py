import pyshark
import mysql.connector


class Activity:
    def __init__(self, timestamp, packets, protocol, src, dst):
        self.timestamp = timestamp
        self.packets = packets
        self.protocol = protocol
        self.src = src
        self.dst = dst

    def set_technique(self, technique):
        self.technique = technique

    def set_ports(self, list_ports):
        self.ports = list_ports


class Sniffer:

    NULL = 0x00  # * 0
    FIN = 0x01  # * 1
    SYN = 0x02  # * 2
    RST = 0x04  # * 4
    PSH = 0x08  # * 8
    ACK = 0x10  # * 16
    URG = 0x20  # * 32

    def __init__(self, network_interface, blocked_ip):
        self.interface = network_interface
        self.ip = blocked_ip
        self.filters = "tcp or udp or icmp"
        self.exporter = Exporter()

    def sniff(self):
        capture = pyshark.LiveCapture(
            interface=self.interface, bpf_filter=self.filters
        )
        udp_packet = []
        tcp_packet = []
        i = 1
        syn = 0
        synpacket = dict()
        print("Welcome! Pyshark will start monitoring your SDN system")
        for packet in capture.sniff_continuously():
            if packet.transport_layer is None and udp_packet:
                activity = Activity(
                    udp_packet[0].sniff_time,
                    udp_packet,
                    "udp",
                    udp_packet[0].ip.src,
                    udp_packet[0].ip.dst,
                )
                print(f"\n{i}. Detect Nmap on {udp_packet[0].sniff_time}")
                self.identify(activity)
                udp_packet = []
            elif packet.transport_layer == "UDP":
                if int(packet.udp.length) <= 20 and packet.ip.src != self.ip:
                    udp_packet.append(packet)
            elif packet.transport_layer == "TCP":
                try:
                    flag = int(packet.tcp.flags, 16)
                    if len(packet.layers) <= 3 and packet.ip.src != self.ip:
                        if int(packet.ip.len) <= 60 and (
                            flag != 4 or flag != 20
                        ):
                            if self.SYN & flag:
                                syn += 1
                                synpacket[packet.tcp.dstport] = False
                            elif self.ACK & flag:
                                if syn > 0:
                                    if packet.ip.dst == self.ip:
                                        if (
                                            packet.tcp.dstport in synpacket
                                            and synpacket[packet.tcp.dstport]
                                            == False
                                        ):
                                            synpacket[
                                                packet.tcp.dstport
                                            ] = True
                                            syn -= 1
                            tcp_packet.append(packet)
                        else:
                            tcp_packet = []
                    if (
                        (self.RST & flag)
                        or (self.FIN & flag)
                        or (self.NULL == flag)
                    ) and tcp_packet:
                        if (
                            packet.tcp.dstport in synpacket
                            and synpacket[packet.tcp.dstport] == False
                        ):
                            synpacket[packet.tcp.dstport] = True
                            syn -= 1
                        if syn == 0:
                            print(
                                f"\n{i}. Detect Nmap on {tcp_packet[0].sniff_time}"
                            )
                            i += 1
                            activity = Activity(
                                tcp_packet[0].sniff_time,
                                tcp_packet,
                                "tcp",
                                tcp_packet[0].ip.src,
                                tcp_packet[0].ip.dst,
                            )
                            self.identify(activity)
                            tcp_packet = []
                        else:
                            if packet.ip.src == self.ip:
                                if (
                                    packet.tcp.srcport in synpacket
                                    and synpacket[packet.tcp.srcport] == False
                                ):
                                    synpacket[packet.tcp.srcport] = True
                                    syn -= 1
                except:
                    pass

    def identify(self, activity):
        list_ports = dict()
        dst_ip = self.ip
        first_packet = activity.packets[0]
        print("   Examining packet from", first_packet.ip.src, "...")
        flag = ""
        packets = activity.packets
        protocol = activity.protocol
        for packet in packets:
            if protocol == "tcp":
                flag = int(packet.tcp.flags, 16)
            src = packet[protocol].srcport
            dst = packet[protocol].dstport
            if dst_ip == packet.ip.dst:
                if dst not in list_ports:
                    list_ports[dst] = []
                # print("atas")
                # print(flag)
                if protocol == "tcp" and flag not in list_ports[dst]:
                    list_ports[dst].append(flag)
            elif dst_ip == packet.ip.src:
                if src not in list_ports:
                    list_ports[src] = []
                # print("bawah")
                # print(flag)
                if protocol == "tcp" and flag not in list_ports[src]:
                    list_ports[src].append(flag)
        print("  ", list_ports)
        if protocol == "tcp" and list_ports:
            selectedFlags = []
            for curPort in list_ports:
                if len(list_ports[curPort]) > len(selectedFlags):
                    selectedFlags = list_ports[curPort]
            # first_port = next(iter(list_ports))
            flags = selectedFlags
            try:
                teknik = self.find_tcp_techniques(flags)
            except:
                print("   Nothing. It's just random packets")
                list_ports = dict()
                pass
        else:
            teknik = "UDP SCAN"
        if list_ports:
            print(
                "   Affirmative! Identifying Nmap activity from",
                first_packet.ip.src,
                "...",
            )
            activity.set_ports(list_ports)
            activity.set_technique(teknik)
            self.export(activity)
        else:
            print("   It's not Nmap! You can now rest in peace :)")

    def find_tcp_techniques(self, flags):
        scan = flags[0]
        if scan == 2:
            if 16 in flags:  # * three-way handshake
                scan = 2  # * connect scan
            else:
                scan = 3  # * syn scan
        if scan == 0:
            nmap = "NULL SCAN"
        elif scan == 1:
            nmap = "FIN SCAN"
        elif scan == 2:
            nmap = "CONNECT SCAN"
        elif scan == 3:
            nmap = "STEALTH SCAN"
        elif scan == 16:
            nmap = "ACK / WINDOW SCAN"
        elif scan == 17:
            nmap = "MAIMON SCAN"
        elif scan == 41:
            nmap = "XMAS SCAN"
        print("  ", nmap)
        return nmap

    def export(self, activity):
        teknik = activity.technique
        if teknik != "":
            try:
                self.exporter.exportDB(activity)
            except:
                print(
                    "   Error inserting to database...Please check your configuration database!"
                )


class Exporter:
    def __init__(self):
        self.db = mysql.connector.connect(
            host="localhost",
            database="sdn_port_scanning",
            user="****",
            password="****",
        )

    def exportDB(self, activity):
        print(
            "   Inserting to database...you may see the result in web interface in about 5 sec... :)"
        )
        nmap = activity.technique
        cursor = self.db.cursor()

        """
        * Cari id Teknik berdasarkan nama tekniknya
        """
        sql = "SELECT `idT` FROM `teknik` WHERE `nama_teknik` = %s"
        teknik = (nmap,)
        cursor.execute(sql, teknik)
        idTeknik = cursor.fetchone()[0]

        """
        * Masukkan idTeknik bersama dengan src, dst, dan timestamp
        """
        query_insert = "INSERT INTO `log` (`source`, `destination`, `timestamp`, `idT`) VALUES (%s, %s, %s, %s)"
        query_values = (
            activity.src,
            activity.dst,
            activity.timestamp,
            idTeknik,
        )
        cursor.execute(query_insert, query_values)
        self.db.commit()

        """
        * Ambil idLog untuk memasukkan port yang di scanned
        """
        idLog = cursor.lastrowid
        query_insert = "INSERT INTO `log_port` (`idL`, `port`) VALUES (%s, %s)"
        query_values = []
        for key in activity.ports:
            query_values.append((idLog, int(key)))
        cursor.executemany(query_insert, query_values)
        self.db.commit()

        """
        * Close connection
        """
        cursor.close()
        return True


sniffer = Sniffer("veth-server", "192.168.0.2")
sniffer.sniff()
