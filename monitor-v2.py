import pyshark
import mysql.connector

NULL = 0x00  # * 0
FIN = 0x01  # * 1
SYN = 0x02  # * 2
RST = 0x04  # * 4
PSH = 0x08  # * 8
ACK = 0x10  # * 16
URG = 0x20  # * 32


def capture_live_packets(network_interface, blocked_ip):
    capture = pyshark.LiveCapture(
        interface=network_interface, bpf_filter="tcp or udp or icmp"
    )
    udp_packet = []
    tcp_packet = []
    for packet in capture.sniff_continuously():
        if packet.transport_layer is None and udp_packet:
            identify(udp_packet, "udp", blocked_ip)
            udp_packet = []
        elif packet.transport_layer == "UDP":
            if int(packet.udp.length) <= 20:
                udp_packet.append(packet)
        elif packet.transport_layer == "TCP":
            try:
                flag = int(packet.tcp.flags, 16)
                if (
                    (RST & flag) or (FIN & flag) or (NULL & flag)
                ) and tcp_packet:
                    print(tcp_packet)
                    identify(tcp_packet, "tcp", blocked_ip)
                    tcp_packet = []
                elif len(packet.layers) <= 3:
                    if int(packet.ip.len) <= 40:
                        tcp_packet.append(packet)
                else:
                    tcp_packet = []
            except:
                pass


def identify(packets, protocol, blocked_ip):
    list_ports = dict()
    dst_ip = blocked_ip
    first_packet = ""
    flag = ""
    scan = protocol
    is_first_packet = True
    for packet in packets:
        if packet.ip.src == blocked_ip:
            continue
        print("examining packet..")
        if is_first_packet:
            first_packet = packet
            is_first_packet = False
        if protocol == "tcp":
            flag = int(packet.tcp.flags, 16)
        src = packet[protocol].srcport
        dst = packet[protocol].dstport
        if dst_ip == packet.ip.dst:
            if dst not in list_ports:
                list_ports[dst] = []
            if protocol == "tcp" and flag not in list_ports[dst]:
                list_ports[dst].append(flag)
        elif dst_ip == packet.ip.src:
            if src not in list_ports:
                list_ports[src] = []
            if protocol == "tcp" and flag not in list_ports[src]:
                list_ports[src].append(flag)
    if protocol == "tcp" and list_ports:
        first_port = next(iter(list_ports))
        flags = list_ports[first_port]
        scan = find_techniques(flags)
    if list_ports:
        print("exporting...")
        export(first_packet, list_ports, scan, protocol)


def find_techniques(flags):
    scan = flags[0]
    if scan == 2:
        if 16 in flags:  # * three-way handshake
            scan = 2  # * connect scan
        else:
            scan = 3  # * syn scan
    return scan


def export(packet, list_ports, scan, protocol):
    if scan == 0:
        nmap = "NULL SCAN"
    elif scan == 1:
        nmap = "FIN SCAN"
    elif scan == 2:
        nmap = "CONNECT SCAN"
    elif scan == 3:
        nmap = "STEALTH SCAN"
    elif scan == 16:
        nmap = "ACK / WINDOW SCAN"
    elif scan == 17:
        nmap = "MAIMON SCAN"
    elif scan == 41:
        nmap = "XMAS SCAN"
    else:
        nmap = "UDP SCAN"
    if scan != 20 and scan != 4:
        timestamp = packet.sniff_time
        src = packet.ip.src
        dst = packet.ip.dst
        exportDB(src, dst, timestamp, nmap, list_ports)


def exportDB(src, dst, timestamp, nmap, ports):
    print("inserting to database...")
    print(src)
    print(dst)
    print(timestamp)
    print(nmap)
    print(ports)
    db = mysql.connector.connect(
        host="localhost",
        database="sdn_port_scanning",
        user="****",
        password="****",
    )
    cursor = db.cursor()

    """
    * Cari id Teknik berdasarkan nama tekniknya
    """
    sql = "SELECT `idT` FROM `teknik` WHERE `nama_teknik` = %s"
    teknik = (nmap,)
    cursor.execute(sql, teknik)
    idTeknik = cursor.fetchone()[0]

    """
    * Masukkan idTeknik bersama dengan src, dst, dan timestamp
    """
    query_insert = "INSERT INTO `log` (`source`, `destination`, `timestamp`, `idT`) VALUES (%s, %s, %s, %s)"
    query_values = (src, dst, timestamp, idTeknik)
    cursor.execute(query_insert, query_values)
    db.commit()

    """
    * Ambil idLog untuk memasukkan port yang di scanned
    """
    idLog = cursor.lastrowid
    query_insert = "INSERT INTO `log_port` (`idL`, `port`) VALUES (%s, %s)"
    query_values = []
    for key in ports:
        query_values.append((idLog, int(key)))
    cursor.executemany(query_insert, query_values)
    db.commit()

    """
    * Close connection
    """
    cursor.close()
    db.close()


capture_live_packets("veth-server", "192.168.0.2")
