-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 24, 2021 at 08:47 PM
-- Server version: 8.0.23-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sdn_port_scanning`
--

-- --------------------------------------------------------

--
-- Table structure for table `Log`
--

CREATE TABLE `Log` (
  `IdL` int NOT NULL,
  `Source` varchar(20) NOT NULL,
  `Destination` varchar(20) NOT NULL,
  `Timestamp` timestamp NOT NULL,
  `IdT` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `LogPort`
--

CREATE TABLE `LogPort` (
  `IdL` int NOT NULL,
  `Port` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Teknik`
--

CREATE TABLE `Teknik` (
  `IdT` int NOT NULL,
  `NamaTeknik` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Protokol` varchar(5) NOT NULL,
  `Flags` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `Teknik`
--

INSERT INTO `Teknik` (`IdT`, `NamaTeknik`, `Protokol`, `Flags`) VALUES
(1, 'STEALTH SCAN', 'TCP', 'SYN, SYN ACK, RST'),
(2, 'CONNECT SCAN', 'TCP', 'SYN, SYN ACK, ACK, RST ACK'),
(3, 'NULL SCAN', 'TCP', 'NULL, RST ACK'),
(4, 'FIN SCAN', 'TCP', 'FIN, RST ACK'),
(5, 'XMAS SCAN', 'TCP', 'FIN PSH URG, RST ACK'),
(6, 'ACK / WINDOW SCAN', 'TCP', 'ACK, RST'),
(7, 'MAIMON SCAN', 'TCP', 'FIN ACK, RST'),
(8, 'UDP SCAN', 'UDP', 'UDP, ICMP Unreachable');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Log`
--
ALTER TABLE `Log`
  ADD PRIMARY KEY (`IdL`),
  ADD KEY `IdT` (`IdT`);

--
-- Indexes for table `LogPort`
--
ALTER TABLE `LogPort`
  ADD KEY `IdL` (`IdL`);

--
-- Indexes for table `Teknik`
--
ALTER TABLE `Teknik`
  ADD PRIMARY KEY (`IdT`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Log`
--
ALTER TABLE `Log`
  MODIFY `IdL` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Teknik`
--
ALTER TABLE `Teknik`
  MODIFY `IdT` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Log`
--
ALTER TABLE `Log`
  ADD CONSTRAINT `Log_ibfk_1` FOREIGN KEY (`IdT`) REFERENCES `Teknik` (`IdT`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `LogPort`
--
ALTER TABLE `LogPort`
  ADD CONSTRAINT `LogPort_ibfk_1` FOREIGN KEY (`IdL`) REFERENCES `Log` (`IdL`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
